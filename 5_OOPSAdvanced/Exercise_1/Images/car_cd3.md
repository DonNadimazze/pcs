@startuml
interface ICarFactory {
  +{abstract} Car Create(color, model)
}
note left of ICarFactory: Create()\n  creates an instance of a car

class Ford {
  +Car* Create(const string& color, const string& model = "Mustang")
}
class Toyota {
  +Car* Create(const string& color, const string& model = "Prius")
}
class Volkswagen{
  +Car* Create(const string& color, const string& model = "Golf")
}

class Car {
  .. Attribute ..
  +double power
  +string produced
  +string model
  +string color
  .. Methods ..
  +Car()
  +Car(string producer, string model, string color)
  +Car(const Car& car)
  +string Show()
  +void SetPower(const double& _power)
  __ operator overloading __
    +Car& operator =(const Car& car);
    +Car& operator +=(const Car& car);
    +Car operator +(const Car& car);
    +bool operator<(const Car& l, const Car& r)
    +friend ostream& operator<<(ostream& stream, const Car& car)
}
note left of Car: Show()\n  returns the color, producer\n  and model of the car


ICarFactory <|.. Ford  : implements
ICarFactory <|.. Toyota  : implements
ICarFactory <|.. Volkswagen  : implements
Car "*" <-- "1" ICarFactory : creates 

@enduml
