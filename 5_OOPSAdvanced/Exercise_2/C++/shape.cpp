#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
        //pi greco c'è e si chiama M_PI
      return 2*PI*sqrt((_a*_a + _b*_b)/2);
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    for(unsigned int i = 0, n = points.size(); i < n; i++)
        perimeter += (points[(i+1)%n]-points[i]).ComputeNorm2();
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge):
      Triangle(p1, p1+ Point(edge, 0), p1 + Point(0.5*edge, 0.5*sqrt(3)*edge)) { }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    for(unsigned int i = 0, n = points.size(); i < n; i++)
        perimeter += (points[(i+1)%n]-points[i]).ComputeNorm2();
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height):
      Quadrilateral(p1, p1 + Point(base, 0), p1 + Point(base, height), p1 + Point(0, height)) { }


  Point Point::operator+(const Point& point) const { return Point(X + point.X, Y + point.Y); }

  Point Point::operator-(const Point& point) const { return Point(X - point.X, Y - point.Y); }

  Point&Point::operator-=(const Point& point)
  {
    X -= point.X;   Y -= point.Y;
    return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;   Y += point.Y;
      return *this;
  }
}
