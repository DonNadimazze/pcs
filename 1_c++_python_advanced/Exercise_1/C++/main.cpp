#include <iostream>
#include "Eigen"  //<.> cerca la libreria tra le standard, "." nella cartella del main

using namespace std;
using namespace Eigen;


/// \brief shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
MatrixXd Shape(const int& n,
               const int& m = 0);
/// \brief rand(n,m) is the n-by-m matrix with random elements.
MatrixXd Rand(const int& n,
              const int& m = 0);
/// \brief hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
MatrixXd Hilb(const int& n,
              const int& m = 0);
///
/// \brief solveSystem(A) solve the linear system Ax=b with x ones.
/// \param detA the determinant of A
/// \param condA the condition number of A
/// \param errRel the realtive error
void SolveSystem(const MatrixXd& A,
                 double& detA,
                 double& condA,
                 double& errRel);

int main()
{
  int n = 10;

  double detAS, condAS, errRelS;
  SolveSystem(Shape(n, n), detAS, condAS, errRelS);
  cout<< scientific<< "shape - DetA: "<< detAS<< ", RCondA: "<< 1.0 / condAS<< ", Relative Error: "<< errRelS<< endl;

  double detAR, condAR, errRelR;
  SolveSystem(Rand(n, n), detAR, condAR, errRelR);
  cout<< scientific<< "rand - DetA: "<< detAR<< ", RCondA: "<< 1.0 / condAR<< ", Relative Error: "<< errRelR<< endl;

  double detAH, condAH, errRelH;
  SolveSystem(Hilb(n, n), detAH, condAH, errRelH);
  cout<< scientific<< "hilb - DetA: "<< detAH<< ", RCondA: "<< 1.0 / condAH<< ", Relative Error: "<< errRelH<< endl;

  return 0;
}


MatrixXd Shape(const int& n, const int& m)
{
    MatrixXd A = MatrixXd::Zero(n, m);      //funzione statica della classe MatrixXd (progr. a oggetti)
    //A(1, 2)= 0.2;
    //A.row(0) << 1, 2, 3, 4;
    //A.col(2) << 8.4, 7, 8, 45;

    int counter=1;
    for (int i=0; i<n; i++)
    {
         for (int j=0; j<n; j++)
         {
             A(i, j)= counter++;
         }
    }

    cout<<A<<endl<<endl;

  return A;
}

MatrixXd Rand(const int& n, const int& m)
{
    MatrixXd A = MatrixXd::Random(n,m);
    cout<<A<<endl<<endl;

  return A;
}


MatrixXd Hilb(const int& n, const int& m)
{
    MatrixXd A = MatrixXd::Zero(n, m);      //funzione statica della classe MatrixXd (progr. a oggetti)

    int counter=1;
    for (int i=0; i<n; i++)
    {
         for (int j=0; j<n; j++)
         {
             A(i, j)= 1.0/(i+j+1);
         }
    }
    cout<<A<<endl<<endl;


  return A;
}

void SolveSystem(const MatrixXd& A,
                 double& detA,
                 double& condA,
                 double& errRel)
{
    int dim = A.rows();  //esiste anche A.cols()

    VectorXd b = VectorXd::Zero(dim);
    b = A.rowwise().sum(); //vedi la mia matrice per ciascuna riga e poi somma, esiste anche A.colwise().sum()

    //PA=LU

   VectorXd x = A.fullPivLu().solve(b);

    cout<< b<<endl<<endl;
    cout<< x<<endl<<endl;


  detA = A.determinant();

  //cond= valore singolare max/val sing min
  JacobiSVD<MatrixXd> svd(A);    //tipo di funzione associato ai template
  cout<< svd.singularValues()<<endl<<endl;

  condA = svd.singularValues()[0]/svd.singularValues()[dim - 1];

  VectorXd solution = VectorXd::Ones(dim);
  cout<<solution<<endl<<endl;
  errRel = (solution - x).norm()/solution.norm() ;
}
