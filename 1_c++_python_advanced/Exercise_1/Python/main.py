import numpy as np
import scipy as sp
from scipy.linalg import lu_factor, lu_solve
#operazione ridondante

def shape(n: int, m: int = 0):
    """
    shape   Shaped matrix.
        shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
    """
    A = np.zeros((n, m))

    #A[0, 2]= 0.1 #assegna alla coordinata
    #A[1]= [2,3]  #assegna a un'intera riga
    #A[:,3] = [...]      #assegna a un'unica colonna

    counter = 1
    for i in range (0, n):
        for j in range (0, m):
            A[i, j] = counter
            counter += 1

    return A


def rand(n: int, m: int = 0):
    """
    rand   Random matrix.
        rand(n,m) is the n-by-m matrix with random elements.
    """
    A = np.random.rand(n, m)

    return A


def hilb(n: int, m: int = 0):
    """
    hilb   Hilbert matrix.
       hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
       it is a famous example of a badly conditioned matrix.
       cond(hilb(n)) grows like exp(3.5*n).
       hilb(n) is symmetric positive definite, totally positive, and a
       Hankel matrix.
    """
    A = np.zeros((n,m))

    for i in range(0, n):
        for j in range(0, m):
            A[i, j] = 1.0 / (i+j+1)
    return A


def solveSystem(A):
    """
    solveSystem   Solve system with matrix.
       solveSystem(A) solve the linear system Ax=b with x ones.
       returns the determinant, the condition number and the relative error
    """
    dim = A.shape[0]    #o A.shape[1] se voglio le colonne
    #b = np.zeros((dim, 1))
    b = A.sum(axis=1)[:, np.newaxis]    #se axis=1 sommo per righe, se =0 sommo per colonne
    #esce fuori vettore riga, serve portarlo in colonna con le par. quadre
    #print(A.shape)

    lu, piv = lu_factor(A)
    x = lu_solve((lu, piv), b)

    solution = np.ones((dim, 1))
    print(A)
    print(b)
    print(x)

    detA = np.linalg.det(A)
    condA = np.linalg.cond(A)
    errRelA = np.linalg.norm(solution - x)/(np.linalg.norm(solution))

    return detA, condA, errRelA


if __name__ == '__main__':
    n: int = 10

    [detAS, condAS, errRelS] = solveSystem(shape(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('shape', detAS, 1. / condAS, errRelS))
    [detAR, condAR, errRelR] = solveSystem(rand(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('rand', detAR, 1. / condAR, errRelR))
    [detAH, condAH, errRelH] = solveSystem(hilb(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('hilb', detAH, 1. / condAH, errRelH))

    exit(0)
