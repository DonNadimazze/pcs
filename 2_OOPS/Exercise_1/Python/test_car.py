from unittest import TestCase  #python ha già una libreria per fare test

import car as car_library


class TestCar(TestCase):      #ciascun test non è una macro, ma una vera e propria classe
    def test_show(self):            #sottosezione diventa nome del metodo
        car = car_library.Car("Fiat", "Mustang", "Red")
        self.assertEqual(car.show(), "Mustang (Fiat): color Red")


class TestCarFactory(TestCase):
    def test_create_ford(self):
        car_library.CarFactory.start_production(car_library.CarProducer.FORD)
        car = car_library.CarFactory.create("Red")
        self.assertTrue(car)
        self.assertEqual(car.show(), "Mustang (Ford): color Red")

    def test_create_toyota(self):
        car_library.CarFactory.start_production(car_library.CarProducer.TOYOTA)
        car = car_library.CarFactory.create("Red")
        self.assertTrue(car)
        self.assertEqual(car.show(), "Prius (Toyota): color Red")

    def test_create_volkswagen(self):
        car_library.CarFactory.start_production(car_library.CarProducer.VOLKSWAGEN)
        car = car_library.CarFactory.create("Red")
        self.assertTrue(car)
        self.assertEqual(car.show(), "Golf (Volkswagen): color Red")

