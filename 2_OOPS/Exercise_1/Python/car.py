from enum import Enum


class Car:          #implementazione e dichiarazione vanno nella stessa classe
    def __init__(self, producer: str, model: str, color: str):     #potevo anche non specificarlo col duck typing
        self.__producer = producer
        self.__model = model
        self.__color = color            #self perchè attributo più doppio underscore per privato

    def show(self) -> str:
        return self.__model + " (" + self.__producer + "): color " + self.__color


class CarProducer(Enum):           #l'enumerazione in python è una vera e propria classe
    UNKNOWN = 0
    FORD = 1
    TOYOTA = 2
    VOLKSWAGEN = 3


class CarFactory:
    __producer = CarProducer.UNKNOWN    #tutti gli attributi fuori dal costruttore sono statici, e poichè statici inizializzati

    @staticmethod
    def start_production(producer: CarProducer):
        CarFactory.__producer = producer

    @staticmethod
    def create(color: str) -> Car:
        if CarFactory.__producer == CarProducer.FORD:
            return Car("Ford", "Mustang", color)
        elif CarFactory.__producer == CarProducer.TOYOTA:
            return Car("Toyota", "Prius", color)
        elif CarFactory.__producer == CarProducer.VOLKSWAGEN:
            return Car("Volkswagen", "Golf", color)
        else:
            #return None oppure lancia eccezione
            raise ValueError("unknown producer")

