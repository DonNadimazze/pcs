#ifndef CAR_H
#define CAR_H

#include <iostream>

using namespace std;

namespace CarLibrary {

  class Car
  {
    private:
      string _producer;
      string _model;
      string _color;     //creazione attributi privati, trattino per stile

    public:
      Car(const string& producer,
          const string& model,
          const string& color);

      string Show();    //tasto destro sul metodo, refactor, add definition in car.cpp

  };


  enum class CarProducer  //come se fossero classi
  {
    UNKNOWN = 0,        //come se fossero attributi
    FORD = 1,
    TOYOTA = 2,
    VOLKSWAGEN = 3
  };

  class CarFactory
  {
    private:
      static CarProducer _producer;     //attributo statico dev'essere inizializzato

    public:
      static void StartProduction(const CarProducer& producer);
      static Car* Create(const string& color);
  };
}

#endif // CAR_H
