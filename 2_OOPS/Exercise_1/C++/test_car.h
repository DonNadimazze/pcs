#ifndef __TEST_CAR_H
#define __TEST_CAR_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "car.h"

using namespace testing;

        //questi test sono stati fatti a prescindere dall'implementazione, ma a partire dall'UML
TEST(TestCar, TestShow)         //unit test, chiamata a funzione macro, sezione (che identifica la classe che vado a testare) e sottosezione (testa metodi)
{
  CarLibrary::Car car = CarLibrary::Car("Fiat", "Mustang", "Red");
  EXPECT_EQ(car.Show(), "Mustang (Fiat): color Red");        //chiamata a funzione macro, si aspetta un'uguaglianza
}

TEST(TestCarFactory, TestCreateFord)
{
  CarLibrary::CarFactory::StartProduction(CarLibrary::CarProducer::FORD);
  CarLibrary::Car* car = CarLibrary::CarFactory::Create("Red");          //creo puntatore a classe
  EXPECT_TRUE(car != nullptr);          //mi aspetto che il puntatore sia diverso da null
  EXPECT_EQ(car != nullptr ? car->Show() : "", "Mustang (Ford): color Red");
  delete car;
}

TEST(TestCarFactory, TestCreateToyota)
{
  CarLibrary::CarFactory::StartProduction(CarLibrary::CarProducer::TOYOTA);
  CarLibrary::Car* car = CarLibrary::CarFactory::Create("Red");
  EXPECT_TRUE(car != nullptr);
  EXPECT_EQ(car != nullptr ? car->Show() : "", "Prius (Toyota): color Red");
  delete car;
}

TEST(TestCarFactory, TestCreateVolkswagen)
{
  CarLibrary::CarFactory::StartProduction(CarLibrary::CarProducer::VOLKSWAGEN);
  CarLibrary::Car* car = CarLibrary::CarFactory::Create("Red");
  EXPECT_TRUE(car != nullptr);
  EXPECT_EQ(car != nullptr ? car->Show() : "", "Golf (Volkswagen): color Red");
  delete car;
}

#endif // __TEST_CAR_H
