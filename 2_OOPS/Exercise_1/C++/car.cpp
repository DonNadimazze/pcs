#include "car.h"

namespace CarLibrary {

  Car::Car(const string &producer,
           const string &model,
           const string &color)
  {
    _producer = producer;   //sono private, ma posso utilizzarle solo perchè sono dentro la stessa classe
    _model = model;
    _color = color;
  }

  string Car::Show()
  {
    return _model + " (" + _producer + "): color " + _color;
  }

  CarProducer CarFactory::_producer = CarProducer::UNKNOWN;  //inizializzazione attributo statico,

  void CarFactory::StartProduction(const CarProducer &producer)
  {
      _producer = producer;
  }

  Car *CarFactory::Create(const string &color)
  {
      switch(CarFactory::_producer)
      {
         case CarProducer::FORD:
           return new Car("Ford", "Mustang", color);
         case CarProducer::TOYOTA:
           return new Car("Toyota", "Prius", color);
         case CarProducer::VOLKSWAGEN:
           return new Car("Volkswagen", "Golf", color);
         default:      //lanciare un'eccezione con un messaggio
           throw runtime_error("Unknown producer");

      }

  }





}
