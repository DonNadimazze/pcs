#include "car2.h"

namespace CarLibrary2 {

Car::Car(const string &producer,
         const string &model,
         const string &color)
{
    _producer = producer;
    _model = model;
    _color = color;
}

string Car::Show()
{
    return _model + " (" + _producer + "): color " + _color;
}

Car *Ford::Create(const string &color)
{
    return new Car("Ford", "Mustang", color);
}

Car *Toyota::Create(const string &color)
{
    return new Car("Toyota", "Prius", color);
}

Car *Volkswagen::Create(const string &color)
{
    return new Car("Volkswagen", "Golf", color);
}

}
