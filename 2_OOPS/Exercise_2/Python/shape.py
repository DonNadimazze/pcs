import math


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b

    def area(self) -> float:
        return self._a*self._b*math.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)

    # def area(self) -> float:
    #    return super().area()


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3

    def area(self) -> float:
        return abs(self._p1.x * (self._p2.y - self._p3.y) + self._p1.y * (self._p3.x - self._p2.x) + self._p2.x * self._p3.y - self._p3.x * self._p2.y) / 2


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self._p1 = p1
        self._edge = edge

    def area(self) -> float:
        return self._edge*self._edge*math.sqrt(3)/4


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4

    def area(self) -> float:
        return abs((self._p1.x - self._p3.x) * (self._p2.y - self._p4.y) - (self._p2.x - self._p4.x) * (self._p1.y - self._p3.y)) / 2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, Point(p2.x + p4.x - p1.x, p2.y + p4.y - p1.y), p4)

    # def area(self) -> float:
    #    return super().area()


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self._p1 = p1
        self._base = base
        self._height = height

    def area(self) -> float:
        return self._base*self._height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)

    # def area(self) -> float:
    #    return super().area()
