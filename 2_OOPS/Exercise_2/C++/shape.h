#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point
  {
    public:
      double _x = 0;
      double _y = 0;

    public:
      Point(const double& x,
            const double& y);
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      int _a, _b;

    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    /*private:
      Point _center = Point(0.0, 0.0);
      int _radius = 0;*/

    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };


  class Triangle : public IPolygon
  {
    private:
      Point _p1, _p2, _p3;

    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);
      double Area() const;
  };

  class TriangleEquilateral : public IPolygon
  {
    private:
      Point _p1;
      int _edge;

    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };


  class Quadrilateral : public IPolygon
  {
    protected:
      Point _p1, _p2, _p3, _p4;

    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };

  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public IPolygon
  {
    protected:
      Point _p1 = Point(0.0, 0.0);
      int _base, _height;

    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
