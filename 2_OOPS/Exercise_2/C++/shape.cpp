#include "shape.h"
#include <math.h>

#define PI 3.14159265359

namespace ShapeLibrary {

//Point class constructor
Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;
}

//Point class copy constructor
Point::Point(const Point &point)
{
    _x = point._x;
    _y = point._y;
}


//ellipse constructor and area method
Ellipse::Ellipse(const Point& center, const int &a, const int &b): _center(center)
{
    _a = a;
    _b = b;
}

double Ellipse::Area() const { return _a*_b*PI; }


//circle constructor and Area method through the ellipse class' ones
Circle::Circle(const Point &center, const int &radius):Ellipse(center, radius, radius) { }

double Circle::Area() const { return Ellipse::Area(); }



Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3): _p1(p1), _p2(p2), _p3(p3) {}

double Triangle::Area() const
{
    return abs(_p1._x*(_p2._y-_p3._y)+_p1._y*(_p3._x-_p2._x)+_p2._x*_p3._y-_p3._x*_p2._y)/2;
}



TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge):_p1(p1) { _edge = edge; }

double TriangleEquilateral::Area() const { return sqrt(3.0)*_edge*_edge/4; }



Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4): _p1(p1), _p2(p2), _p3(p3), _p4(p4) {}

double Quadrilateral::Area() const
{
    return abs((_p1._x-_p3._x)*(_p2._y-_p4._y)-(_p2._x-_p4._x)*(_p1._y-_p3._y))/2;
}



Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):
    Quadrilateral(p1, p2, Point(p2._x+p4._x-p1._x, p2._y+p4._y-p1._y), p4) {}

double Parallelogram::Area() const { return Quadrilateral::Area(); }



Rectangle::Rectangle(const Point &p1, const int &base, const int &height): _p1(p1)
{
    _base = base;
    _height = height;
}

double Rectangle::Area() const { return _base*_height; }


//
Square::Square(const Point &p1, const int &edge):Rectangle(p1, edge, edge){}

double Square::Area() const { return Rectangle::Area(); }


}
