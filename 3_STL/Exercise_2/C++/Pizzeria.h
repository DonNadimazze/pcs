#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
  #include <unordered_set>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      string Description;
      int Price;

      Ingredient(const string& name, const string& description, const int& price);
  };

  class Pizza {
    public:
      string Name;
      vector<Ingredient> Ingredients;

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const;
  };

  class Order {
    public:
      //int numOrder;
      vector<Pizza> Pizzas;

      void InitializeOrder(int numPizzas) { throw runtime_error("Unimplemented method"); }
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const;
      int ComputeTotal() const;
  };

  class Pizzeria {
    public:
      vector<Ingredient> ingredients;
      vector<Pizza> pizzas;
      vector<Order> orders;

      void AddIngredient(const string& name, const string& description, const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name, const vector<string>& ingredients);

      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
