#include "Pizzeria.h"
#include <algorithm>

namespace PizzeriaLibrary {

//Ingredient class constructor (ing is contraction)
Ingredient::Ingredient(const string &name, const string &description, const int &price)
{
    Name = name;
    Description = description;
    Price = price;
}


//pizza class methods implementation
void Pizza::AddIngredient(const Ingredient &ingredient) { Ingredients.push_back(ingredient); }
int Pizza::NumIngredients() const { return Ingredients.size(); }

int Pizza::ComputePrice() const
{
    int  total = 0;
    for( unsigned int i = 0; i < (unsigned int)NumIngredients(); i++ )
    {
        total += Ingredients[i].Price;
    }
    return total;
}


//Order class methods implementation
void Order::AddPizza(const Pizza &pizza) { Pizzas.push_back(pizza); }

const Pizza &Order::GetPizza(const int &position) const
{
    if(position < NumPizzas())
        return Pizzas[position];
    throw runtime_error("Position noi found");
}

int Order::NumPizzas() const { return Pizzas.size(); }

int Order::ComputeTotal() const
{
    int  total = 0;
    for(int i = 0; i < NumPizzas(); i++ )
    {
        total += Pizzas[i].ComputePrice();
    }
    return total;
}



void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    try
    {
        FindIngredient(name);
    }
    catch (const exception& exception)
    {
        ingredients.push_back(Ingredient(name, description, price));
        sort(ingredients.begin(), ingredients.end(), [](Ingredient a, Ingredient b) {return a.Name < b.Name; });
        return;

    }
    throw runtime_error("Ingredient already inserted");
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    unsigned int numIngredients = ingredients.size();
    for (unsigned int i = 0; i < numIngredients; i++)
    {
        if (ingredients[i].Name == name)
            return ingredients[i];
    }
    throw runtime_error("Ingredient not found");
}


void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    try
    {
        FindPizza(name);
    }
    catch (const exception& exception)
    {
        Pizza newPizza;
        newPizza.Name = name;
        for(unsigned int i = 0; i < ingredients.size(); i++)
            newPizza.AddIngredient(FindIngredient(ingredients[i]));

        pizzas.push_back(newPizza);
        return;
    }
    throw runtime_error("Pizza already inserted");
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    unsigned int numPizzas = pizzas.size();
    for (unsigned int i = 0; i < numPizzas; i++)
    {
        if (pizzas[i].Name == name)
            return pizzas[i];
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    unsigned int n = pizzas.size();
    if (!n)
        throw runtime_error("Empty order");
    else
    {
        Order newOrder;
        for (unsigned int i = 0; i < n; i++)
            newOrder.AddPizza(FindPizza(pizzas[i]));
        orders.push_back(newOrder);
    }
    return 999 + orders.size();

}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    int n = numOrder - 1000;
    if (n < (int)orders.size())
        return orders[n];
    throw runtime_error("Order not found");
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string receipt;
    Order _order;

    try { _order = FindOrder(numOrder); }

    catch (const exception& exception) { throw runtime_error("Order not found"); }

    for (int i = 0; i < _order.NumPizzas(); i++)
    {

        receipt += "- " + _order.Pizzas[i].Name + ", " + to_string(_order.Pizzas[i].ComputePrice()) + " euro\n";
    }
    receipt += "  TOTAL: " + to_string(_order.ComputeTotal()) + " euro\n";

    return receipt;
}

string Pizzeria::ListIngredients() const
{
    string fullList = "";
    for (unsigned int i=0; i<ingredients.size(); i++)
    {
        fullList += ingredients[i].Name + " - '" + ingredients[i].Description + "': " + to_string(ingredients[i].Price) + " euro\n";;
    }

    return fullList;
}

string Pizzeria::Menu() const
{
    string fullMenu = "";
    for (int i=pizzas.size() - 1; i>=0; i--)
    {
        fullMenu += pizzas[i].Name + " (" + to_string(pizzas[i].NumIngredients()) + " ingredients): " + to_string(pizzas[i].ComputePrice()) + " euro\n";
    }

    return fullMenu;
}







}
