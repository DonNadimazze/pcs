class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.Ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.Ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.Ingredients)

    def computePrice(self) -> int:
        price = 0
        for i in range(0, self.numIngredients()):
            price += self.Ingredients[i].Price
        return price


class Order:
    def __init__(self):
        self.Pizzas = []

    def getPizza(self, position: int) -> Pizza:
        if position < self.numPizzas():
            return self.Pizzas[position]
        raise RuntimeError("Position noi found")

    def initializeOrder(self, numPizzas: int):
        pass

    def addPizza(self, pizza: Pizza):
        self.Pizzas.append((pizza))

    def numPizzas(self) -> int:
        return len(self.Pizzas)

    def computeTotal(self) -> int:
        total = 0
        for i in range(0, self.numPizzas()):
            total += self.Pizzas[i].computePrice()
        return total


class Pizzeria:
    def __init__(self):
        self.AllIngredients = []
        self.AllPizzas = []
        self.AllOrders = []

    def addIngredient(self, name: str, description: str, price: int):
        try:
            self.findIngredient(name)
        except Exception:
            self.AllIngredients.append(Ingredient(name, price, description))
            self.AllIngredients.sort(key=lambda x: x.Name, reverse=False)
            return
        raise RuntimeError("Ingredient already inserted")

    def findIngredient(self, name: str) -> Ingredient:
        for i in range(0, len(self.AllIngredients)):
            if self.AllIngredients[i].Name is name:
                return self.AllIngredients[i]
        raise RuntimeError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        try:
            self.findPizza(name)
        except Exception:
            newPizza = Pizza(name)
            newPizza.Ingredients = [self.findIngredient(ingredients[i]) for i in range(0, len(ingredients))]
            # for i in range(0, len(ingredients)):
            #     x = self.findIngredient(ingredients[i])
            #     newPizza.Ingredients.append(x)
            self.AllPizzas.append(newPizza)
            return
        raise RuntimeError('Pizza already inserted')

    def findPizza(self, name: str) -> Pizza:
        for i in range(0, len(self.AllPizzas)):
            if self.AllPizzas[i].Name is name:
                return self.AllPizzas[i]
        raise RuntimeError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        n = len(pizzas)
        if n == 0:
            raise RuntimeError("Empty order")
        else:
            newOrder = Order()
            newOrder.Pizzas = [self.findPizza(pizzas[i]) for i in range(0, len(pizzas))]
            # for i in range(o, n):
            #     newOrder.addPizza(self.findPizza(pizzas[i]))
            self.AllOrders.append(newOrder)
        return 999 + len(self.AllOrders)

    def findOrder(self, numOrder: int) -> Order:
        n = numOrder - 1000
        if n < len(self.AllOrders):
            return self.AllOrders[n]
        raise RuntimeError("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        fOrder = Order()
        try:
            fOrder = self.findOrder(numOrder)
        except Exception:
            raise RuntimeError("Order not found")
        receipt = ""
        for i in range(0, fOrder.numPizzas()):
            receipt += "- " + fOrder.Pizzas[i].Name + ", " + str(fOrder.Pizzas[i].computePrice()) + " euro\n"
        receipt += "  TOTAL: " + str(fOrder.computeTotal()) + " euro\n"
        return receipt

    def listIngredients(self) -> str:
        fullList = ""
        for i in range(0, len(self.AllIngredients)):
            fullList += self.AllIngredients[i].Name + " - '" + self.AllIngredients[i].Description + "': " + \
                        str(self.AllIngredients[i].Price) + " euro\n"
        return fullList

    def menu(self) -> str:
        fullMenu = ""
        for i in range(0, len(self.AllPizzas)):
            fullMenu += self.AllPizzas[i].Name + " (" + str(self.AllPizzas[i].numIngredients()) + " ingredients): " + \
                        str(self.AllPizzas[i].computePrice()) + " euro\n"
        return fullMenu
