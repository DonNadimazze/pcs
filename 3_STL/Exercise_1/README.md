# Shopping List 

There exists many apps on the phone which allows the user to create a shopping list.

## Example 

The image below shows a simple a Shopping List application

![list](Images/List.png)

## Requirements

Write a software which creates a basic shopping list.
At first creation the list is empty.
The user can interact with the program using the following structure:

![list](Images/list_cd.png)

Implement the interface `IShoppingApp` using the different advanced containers introduced during the course:

* vector
* list
* queue
* stack
* hash
