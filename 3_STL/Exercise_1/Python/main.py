class IShoppingApp:
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        pass

    def undo(self):
        pass

    def reset(self):
        pass

    def searchElement(self, product: str) -> bool:
        return False


class VectorShoppingApp(IShoppingApp):
    #        self.elements = [0] * 100           # reserve(100) in c++
    def __init__(self):                          #in py non esistono vettori in senso proprio, ma liste, la differenza sta nel modo in cui vengono allocate
        self.elements = []                       #(vettori partono da dimensione finita, liste senza dimensione e inserimento in solo append)

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        numElements: int = self.numberElements()
        for i in range(0, numElements):
            if self.elements[i] is product:
                return True
        return False

class ListShoppingApp(IShoppingApp):
    def __init__(self):                     # tutto uguale a vettore tranne ricerca, per cui serve un iteratore
        self.elements = []

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        for element in self.elements:               # passo da un elemento all'altro attraverso l'operatore in
            if element is product:
                return True
        return False


class StackShoppingApp(IShoppingApp):
    def __init__(self):
        self.elements = []

    def numberElements(self) -> int:            #again, non c'è differenza tra vettore, lista, pila, coda
        return len(self.elements)

    def addElement(self, product: str):         #aggiunta in LIFO, .append = .push
        self.elements.append(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:              # la ricerca è più lunga
        stackUndo = []
        found: Bool = False
        numElements = self.numberElements()
        for i in range(0, numElements):
            element = self.elements.pop()
            stackUndo.append(element)
            if element is product:
                found = True
                break
        numElementsUndo = len(stackUndo)
        for i in range(0, numElementsUndo):
            self.elements.append(stackUndo.pop())
        return found



class QueueShoppingApp(IShoppingApp):
    def __init__(self):  # tutto uguale a vettore tranne ricerca, per cui serve un iteratore
        self.elements = []

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):         #OCHO, FIFO
        queueUndo = []
        numElements = self.numberElements()
        for i in range(0, numElements - 1):
            queueUndo.append(self.elements.pop())
        self.elements.pop()
        for i in range(0, numElements - 1):
            self.elements.append(queueUndo.pop())

    def reset(self):
        self.elements.clear()               #o ciclo for per scodare

    def searchElement(self, product: str) -> bool:
        queueUndo = []
        found: Bool = False
        numElements = self.numberElements()
        for i in range(0, numElements):
            element = self.elements.pop()
            queueUndo.append(element)
            if element is product:
                found = True
                break
        numElementsUndo = len(queueUndo)
        for i in range(0, numElementsUndo):
            self.elements.append(queueUndo.pop())
        return found

class HashShoppingApp(IShoppingApp):
    def __init__(self):
        self.elements = set()

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.add(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        return product in self.elements
