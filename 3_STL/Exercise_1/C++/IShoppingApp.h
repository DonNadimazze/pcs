#ifndef SHOPPING_H
#define SHOPPING_H

#include <iostream>     //standard library
#include <vector>       //array dinamico templatizzato
#include <list>         //lista
#include <stack>        //pila, classe templatizzata che ragiona lifo
#include <queue>        //coda, FIFO
#include <unordered_set>//hash, non

using namespace std;

namespace ShoppingLibrary {

  class IShoppingApp {
    public:
      virtual unsigned int NumberElements() const = 0;
      virtual void AddElement(const string& product) = 0;
      virtual void Undo() = 0;
      virtual void Reset() = 0;
      virtual bool SearchElement(const string& product) = 0;
  };

  class VectorShoppingApp : public IShoppingApp {
    private:
      vector<string> elements;

    public:
      void Temp() {
//         elements.resize(10);      //alloca dinamicamente su quel puntatore 10 elementi di quel tipo (fa tutto, new e delete[])
//        elements[5] = "pippo";

         elements.reserve(10);             //non lo posso fare senza conoscere il numero di elementi a priori
         elements.push_back("pippo2");     //aggiungo in coda senza sapere la dimensione del vettore, ma perdiamo l'accesso all'elemento O(1)
                                           //si comporta come se fosse una lista
      }

      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }          //il contrario della push_back, elimina l'ultimo in coda
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)         //non esiste la ricerca nello standard
      {                                                 //implementeremo la ricerca lineare
          unsigned int numElements = NumberElements();
          for (unsigned int i = 0; i < numElements; i++)
          {
              if (elements[i] == product)
                  return true;
          }
          return false;
      }
  };

  class ListShoppingApp : public IShoppingApp {
    private:
      list<string> elements;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product) //non posso direttamente accedere all'i-esimo elemento, serve iteratore associato alla lista di stringhe
      {
            //list<string>::iterator it = elements.begin();   //parte dal puntatore al primo elemento della lista e arriva a quello alla fine della lista

          for(list<string>::iterator it = elements.begin(); it != elements.end(); it++)    //overload operatore ++
          {
              //const string& element = *it;
              if (/*element*/ *it == product)
                  return true;
          }
          return false;
      }
  };


  class StackShoppingApp : public IShoppingApp {
    private:
      stack <string> elements;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() { elements.pop(); }
      void Reset()
      {
          unsigned int numElements = NumberElements();
          for(unsigned int i = 0; i < numElements; i++)
              elements.pop();
      }
      bool SearchElement(const string& product)
      {
          stack<string> stackNew;
          bool found = false;
          unsigned int numElements = NumberElements();
          for(unsigned int i = 0; i < numElements; i++)
          {
              const string& element = elements.top();
              if (element == product)
              {
                  found = true;
                  break;
              }

              stackNew.push(element);
              elements.pop();                       //ho finito di cercare, devo reimpilare
          }
          unsigned int numElementsNew = stackNew.size();
          for(unsigned int i = 0; i < numElementsNew; i++)
          {
              elements.push(stackNew.top());
              stackNew.pop();
          }
          return found;
      }
  };

  class QueueShoppingApp : public IShoppingApp {
    private:
      queue<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo()
      {
          queue<string> queueNew;
          unsigned int numElements = NumberElements();
          for(unsigned int i = 0; i < numElements; i++)
          {
              queueNew.push(elements.front());
              elements.pop();
          }
          elements.pop();
          for(unsigned int i = 0; i < numElements; i++)
          {
              elements.push(queueNew.front());
              queueNew.pop();
          }
      }
      void Reset()
      {
          unsigned int numElements = NumberElements();
          for(unsigned int i = 0; i < numElements; i++)
              elements.pop();
      }
      bool SearchElement(const string& product)
      {
          queue<string> queueNew;
          bool found = false;
          unsigned int numElements = NumberElements();
          for(unsigned int i = 0; i < numElements; i++)
          {
              const string& element = elements.back();
              if (element == product)
              {
                  found = true;
                  break;
              }

              queueNew.push(element);
              elements.pop();
          }
          unsigned int numElementsNew = queueNew.size();
          for(unsigned int i = 0; i < numElementsNew; i++)
          {
              elements.push(queueNew.back());
              queueNew.pop();
          }
          return found;
      }
  };

  class HashShoppingApp : public IShoppingApp {         //hash non ha un'ordinamento come le altre strutture dati, ma è indicizzata da funzione-con-chiave
    private:
      unordered_set<string> elements;
      string lastElement;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { lastElement = product; elements.insert(product); }
      void Undo() { elements.erase(lastElement); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
        { return elements.find(product) != elements.end(); }
  };
}

#endif // SHOPPING_H
