# \brief ImportVectors reads the input vectors from file for dot product
# \param filePath: path name of the input file
# \return the result of the reading, true is success, false is error
# \return n: resulting size of the vectors
# \return v1: resulting vector1
# \return v2: resulting vector2
def importVectors(inputFilePath):
    file = open(inputFilePath, 'r') #nientge ifstream, ofstream, ma come in C

    lines = file.readlines() #legge tutte le linee
    n = int(lines[1])  #operatore di conversione più snello, ma necessario, altrimenti n sarebbe stringa
    #convertitore intrinseco all'interno del tipo       #il casting non fuzniona in c++
    v1 = [int(i) for i in lines[3].split(' ')] #comprehension o zucchero sintattico
    v2 = [int(i) for i in lines[5].split(' ')] #praticamente memoria allocata

    file.close()

    return True, n, v1, v2

# \brief DotProduct performs the dot product between two vectors
# \param n: size of the vectors
# \param v1: the first vector
# \param v2: the second vector
# \return the result of the operation, true is success, false is error
# \return dotProduct: the resulting dot product
def dotProduct(n, v1, v2):
    result = 0
    for i in range (0, n):       #come matlab
        result += v1[i] * v2[i]

    return True, result

# \brief ExportResult export the result obtained in file
# \param outputFilePath: path name of the output file
# \param v1: vector1
# \param v2: vector2
# \param dotProduct: the dot product
# \return the result of the export, true is success, false
def exportResult(outputFilePath, n, v1, v2, dotProduct):

    fileWrite = open(outputFilePath, 'w')
    print("# size of the two vectors", file=fileWrite)
    print(n, file=fileWrite)
    print("# vector 1", file=fileWrite)
#    print(v1, file=file) questo stampa con le quadre
    print(*v1, file=fileWrite)      #questo scrive il contenuto del vettore invece del vettore in sè
    print("# vector 2", file=fileWrite)
    print(*v2, file=fileWrite)
    print("# dot Product", file=fileWrite)
    print(dotProduct, file=fileWrite)


    return True


if __name__ == '__main__':
    inputFileName = "vectors.txt"

    [resultImport, n, v1, v2] = importVectors(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: n=", n, " v1=", v1, " v2=", v2)

    [resultDotProduct, dotProduct] = dotProduct(n, v1, v2)
    if not resultDotProduct:
        print("Something goes wrong with dot product")
        exit(-1)
    else:
        print("Computation successful: result ", dotProduct)

    outputFileName = "dotProduct.txt"
    if not exportResult(outputFileName, n, v1, v2, dotProduct):
        print("Something goes wrong with export")
        exit(-1)
    else:
        print("Export successful")
