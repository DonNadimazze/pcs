#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    SetToleranceIntersection(1.0E-7);
    SetToleranceParallelism(1.0E-5);
    intersectionType = NoInteresection;
}


/*Intersector2D2D::~Intersector2D2D()
{

}*/
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
   matrixNomalVector.row(0) = planeNormal;
   rightHandSide[0] = planeTranslation;

  return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide[1] = planeTranslation;

  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    bool exit = true;
    Vector3d line = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));

    if (line.norm()<ToleranceIntersection()){
        exit = false;
        if(abs(rightHandSide[1]-rightHandSide[0]) < ToleranceIntersection()){
            intersectionType = Coplanar;
        }
        //else intersectionType = NoInteresection;
    }
    else {
        matrixNomalVector.row(2) = line;
        rightHandSide[2] = 0;
        pointLine = matrixNomalVector.colPivHouseholderQr().solve(rightHandSide);
        tangentLine = line;
        intersectionType = LineIntersection;
    };

  return exit;
}
