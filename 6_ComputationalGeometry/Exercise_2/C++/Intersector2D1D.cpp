#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    SetToleranceIntersection(1.0E-7);
    SetToleranceParallelism(1.0E-7);
    intersectionType = NoInteresection;

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
  return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
  return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    bool exit = true;
    double denominator = planeNormalPointer->dot(*lineTangentPointer),
           numerator = *planeTranslationPointer - planeNormalPointer->dot(*lineOriginPointer);
    //ho messo a cazzo le incertezze, il sistema dovrebbe essere di gran lunga più complesso
    if (abs(denominator) < toleranceParallelism)
    {
        if(abs(numerator) > toleranceIntersection)
        {
            intersectionType = NoInteresection;
        }
        else intersectionType = Coplanar;
        exit = false;
    } else {
        intersectionParametricCoordinate = numerator/denominator;
        intersectionType = PointIntersection;
    };

  return exit;
}
